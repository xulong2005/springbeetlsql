package com.park.oss.service.impl;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.beetl.sql.core.Params;
import org.beetl.sql.core.SQLManager;
import org.beetl.sql.ext.spring.SpringBeetlSql;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.park.oss.model.CarPlace;
import com.park.oss.model.Payment;
import com.park.oss.model.PlaceRent;
import com.park.oss.model.SysUser;
import com.park.oss.service.PaymentService;
import com.park.oss.service.PlaceService;
import com.park.oss.util.ParkUtil;

@Service
@Transactional
public class PaymentServiceImpl implements PaymentService {

	@Autowired
	SQLManager sqlManager ;
	
	@Autowired
	PlaceService placeService ;
	
	
	@Override
	public List<Payment> query(Integer parkId) {
		Payment query = new Payment();
		query.setParkId(parkId);
		return sqlManager.template(query,1,100);
	}
	
	@Override
	public List<Payment> queryUser(Integer userId,boolean isUser) {
		Payment query = new Payment();
		if(isUser){
			query.setUserId(userId);
		}else{
			query.setOwnerId(userId);
		}
		
		return sqlManager.select("payment.queryUser", Payment.class, query);
	}

	@Override
	
	public int doRent(SysUser user, Integer parkId) {
//		Integer orderId = checkOrder(user,parkId);
//		if(orderId!=null) return orderId;
		SQLManager sql = sqlManager;
		PlaceRent query = new PlaceRent();
		query.setStatus(0);
		query.setDay(ParkUtil.today());
		query.setParkId(parkId);
		List<PlaceRent> rents = sql.template(query);
		if(rents.size()==0){
			return -1 ;
		}
		PlaceRent rent = null;
		for(PlaceRent r:rents){
			 r = rents.get(0);
			 if(r.getOwnerId()!=user.getId()){
				 rent = r;
			 }
				 
		}
		
		if(rent==null){
			return -2;
		}
		
		rent.setStatus(1);
		// 已订购
		sql.updateById(rent);
		
		//生成订单
		CarPlace place = new CarPlace();
	    	place.setCreateTime(new Timestamp(new Date().getTime()));
	    	place.setMobile(user.getMobile());
	    	place.setPlaceId(rent.getPlaceId());
	    	place.setParkId(parkId);
	    	place.setPlate(user.getPlate());
	    	place.setStatus(0);
	    	place.setPalceRentId(rent.getId());
	    	place.setUserId(user.getId());
	    	place.setOwnerId(rent.getOwnerId());
	    Integer id =	placeService.order(place);
	    return id;
	    
	}
	
	@Override
	public void unRent(CarPlace cp,SysUser user, Integer parkId) {
		//取消订单
		CarPlace myCp = new CarPlace();
		myCp.setId(cp.getId());
		myCp.setStatus(4);
		sqlManager.updateTemplateById(myCp);
		
		// 车位状态改成有空
		PlaceRent rent = new PlaceRent();
		rent.setId(cp.getPalceRentId());
		rent.setStatus(0);
		
		sqlManager.updateTemplateById(rent);
		
	}
	

	@Override
	public void doEnter(SysUser user, Integer parkId) {
		
		CarPlace query = new CarPlace();
		query.setParkId(parkId);
		query.setUserId(user.getId());
		query.setStatus(0);
		List<CarPlace> carPlaces = sqlManager.template(query);
		CarPlace pl = carPlaces.get(0);
		placeService.enter(pl.getId());
		
	}
//	/**
//	 *  当天是否订购过
//	 * @param user
//	 * @param parkId
//	 * @return
//	 */
//	private Integer checkOrder(SysUser user, Integer parkId){
//		SQLManager sql = sqlManager;
//		PlaceRent query = new PlaceRent();
//		query.setDay(ParkUtil.today());
//		query.setParkId(parkId);
//		query.setUserId(user.getId());
//		List<PlaceRent> rents = sql.template(query);
//		if(rents.size()==0){
//			return null;
//		}
//		for(PlaceRent)
//		PlaceRent rent = rents.get(0);
//		return rent.getId();
//	}

	@Override
	@Transactional
	public CarPlace doPay(SysUser user, Integer parkId) {
		SQLManager sql = sqlManager;
		CarPlace query = new CarPlace();
		query.setParkId(parkId);
		query.setUserId(user.getId());
		query.setStatus(1);
		List<CarPlace> carPlaces = sql.template(query);
		CarPlace pl = carPlaces.get(0);
		placeService.pay(pl.getId());
		return pl;
	}
	
	
	

	@Override
	public void doExit(SysUser user, Integer parkId) {
		SQLManager sql = sqlManager;
		CarPlace query = new CarPlace();
		query.setParkId(parkId);
		query.setUserId(user.getId());
		query.setStatus(2);
		List<CarPlace> carPlaces = sql.template(query);
		CarPlace pl = carPlaces.get(0);
		placeService.exit(pl.getId());
	}

	@Override
	public CarPlace getAvaibleRent(Integer userId) {
		CarPlace query = new CarPlace();
		query.setUserId(userId);
		SQLManager sql = sqlManager;
		CarPlace cp = sql.selectSingle("carPlace.selectLatestRent", query, CarPlace.class);
		return cp;
	}


	

}
