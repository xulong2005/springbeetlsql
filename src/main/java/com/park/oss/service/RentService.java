package com.park.oss.service;

import java.util.List;

import com.park.oss.model.PlaceRent;

public interface RentService {
	public List<PlaceRent> query(Integer parkId,Integer userId,Integer day);
	public void updateRent (Integer rentId,Integer parkId,Integer userId,String day,Integer status);
}
