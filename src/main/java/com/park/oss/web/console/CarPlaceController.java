package com.park.oss.web.console;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.park.oss.service.CarPlaceService;
import com.park.oss.vo.CarPlaceVo;

@Controller
@RequestMapping("/carPlace")
public class CarPlaceController {
	@Autowired
	CarPlaceService service ;
    @RequestMapping(value = "/index.html", method = RequestMethod.GET)
    public ModelAndView index(
    		@RequestParam(value="parkId", required=false) Integer parkId,
    		@RequestParam(value="plate", required=false) String plate,
    		@RequestParam(value="mobile", required=false) String mobile
    		) {
    	
    	ModelAndView view = new ModelAndView("/carPlace/index.btl");
    	List<CarPlaceVo> list = service.query(parkId, plate, mobile);
    	view.addObject("list",list);
        return view;
    }
    
    
    
    
}
