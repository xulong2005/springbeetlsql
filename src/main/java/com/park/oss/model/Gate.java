package com.park.oss.model;
import java.math.*;
import java.util.Date;

import org.beetl.sql.core.annotatoin.TableTemplate;

import java.sql.Timestamp;

/*
* 出入口
* gen by beetlsql 2016-01-09
*/
@TableTemplate("order by id desc")
public class Gate  {
	private Integer id ;
	private Integer parkId ;
	private Integer type ; // 1  入口，2 出口
	private String code ;
	private String ip ;
	private Double lat ;
	private Double lon ;
	private String name ;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getParkId() {
		return parkId;
	}
	public void setParkId(Integer parkId) {
		this.parkId = parkId;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public Double getLat() {
		return lat;
	}
	public void setLat(Double lat) {
		this.lat = lat;
	}
	public Double getLon() {
		return lon;
	}
	public void setLon(Double lon) {
		this.lon = lon;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

}
