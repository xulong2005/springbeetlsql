package com.park.oss.model;

import org.beetl.sql.core.TailBean;
import org.beetl.sql.core.annotatoin.TableTemplate;

/*
*  出租计划
* gen by beetlsql 2016-01-12
*/
@TableTemplate("order by id desc ")
public class PlaceRent extends TailBean{
	private Integer id ;
	private Integer end ;
	private Integer ownerId ;
	private Integer userId ;
	private Integer parkId ;
	private Integer placeId ;
	private Integer rentType ;
	private Integer start ;
	//0 未使用 1 使用 2 异常 -1 当天不出租
	private Integer status ;
	private String day ;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getEnd() {
		return end;
	}
	public void setEnd(Integer end) {
		this.end = end;
	}
	public Integer getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(Integer ownerId) {
		this.ownerId = ownerId;
	}
	public Integer getParkId() {
		return parkId;
	}
	public void setParkId(Integer parkId) {
		this.parkId = parkId;
	}
	public Integer getPlaceId() {
		return placeId;
	}
	public void setPlaceId(Integer placeId) {
		this.placeId = placeId;
	}
	public Integer getRentType() {
		return rentType;
	}
	public void setRentType(Integer rentType) {
		this.rentType = rentType;
	}
	public Integer getStart() {
		return start;
	}
	public void setStart(Integer start) {
		this.start = start;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

}
