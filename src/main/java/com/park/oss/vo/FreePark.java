package com.park.oss.vo;

import com.park.oss.model.Park;

public class FreePark extends Park {
	int freeCount;
	double distance = 1000;
	Integer carStatus;

	public int getFreeCount() {
		return freeCount;
	}

	public void setFreeCount(int freeCount) {
		this.freeCount = freeCount;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public Integer getCarStatus() {
		return carStatus;
	}

	public void setCarStatus(Integer carStatus) {
		this.carStatus = carStatus;
	}
	
	
}
