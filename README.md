#springbeetlsql

beetl+springmvc+beetlsql 的一个完整demo，假设了如下业务场景

* 业主可以出租自己车位给物业公司，没有车位的人可以租赁此车位，并付费
* 业主和物业公司都可以获得收入


* 管理系统入口 127.0.0.1:8080/demo/console
* 个人（业主或者司机)  127.0.0.1:8080/demo/mobile，请用手机访问或者浏览器调整为手机模式

本系统beetlsql 位于/src/main/webapp/WEB-INF/lib/beetlsql-2.3.0.jar目录下，用来测试即将发行的beetlsql。


beetlsql的重要特性mapper并未放在演示里，主要是考虑到sqlManager是实质上的beetlsql核心，mapper只是将sql文件映射成一个dao接口做了封装

但在实际项目里，建议用mapper为主。这样更容易维护